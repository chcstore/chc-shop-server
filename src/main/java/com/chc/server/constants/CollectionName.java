package com.chc.server.constants;

public class CollectionName {
    public static final String BRAND = "brands";
    public static final String CATEGORY = "categories";
    public static final String SUPPLIER = "suppliers";
    public static final String PRODUCT = "products";
    public static final String USER = "users";
    public static final String ORDER = "orders";
    public static final String DATABASE_SEQUENCE = "db_sequences";
}
