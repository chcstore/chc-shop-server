package com.chc.server.constants;

public class PrefixId {
    public static final String USER = "user-";
    public static final String BRAND = "brand-";
    public static final String CATEGORY = "category-";
    public static final String SUPPLIER = "supplier-";
    public static final String PRODUCT = "product-";
    public static final String ORDER = "order-";
}
