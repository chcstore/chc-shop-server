package com.chc.server.constants;

public class SessionAttributeContext {
    public static final String SECURITY_CONTEXT = "SPRING_SECURITY_CONTEXT";
    public static final String TOKEN = "TOKEN_SESSION";
    public static final String CART_SESSION = "CART_SESSION";
    public static final String CART_SESSION_QUANTITY= "CART_SESSION_QUANTITY";
    public static final String LAST_ORDERED_SESSION = "LAST_ORDERED_SESSION";
    public static final String IS_ADMIN = "IS_ADMIN";
    public static final String USER_FULLNAME = "USER_FULLNAME_SESSION";
}
