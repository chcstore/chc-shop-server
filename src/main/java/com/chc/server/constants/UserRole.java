package com.chc.server.constants;

public class UserRole {
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String GUEST = "GUEST";
}
