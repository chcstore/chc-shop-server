package com.chc.server.controllers;

import com.chc.server.constants.PrefixId;
import com.chc.server.constants.UserRole;
import com.chc.server.models.User;
import com.chc.server.models.forms.RegisterForm;
import com.chc.server.repositories.UserRepository;
import com.chc.server.services.MailSenderService;
import com.chc.server.services.SequenceGeneratorService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Controller
public class AccountController {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserRepository userRepository;
    private final MailSenderService mailSenderService;
    private final SequenceGeneratorService sequenceGeneratorService;

    public AccountController(BCryptPasswordEncoder bCryptPasswordEncoder,
                             UserRepository userRepository,
                             MailSenderService mailSenderService, SequenceGeneratorService sequenceGeneratorService) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
        this.mailSenderService = mailSenderService;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    // region Register
    @GetMapping("/register")
    public ModelAndView registerPage() {
        ModelAndView modelAndView = new ModelAndView("pages/user/register");

        modelAndView.addObject("registerForm", new RegisterForm());

        return modelAndView;
    }

    @PostMapping("/register")
    public ModelAndView registerProcessing(@Valid @ModelAttribute("registerForm") RegisterForm registerForm,
                                           BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView("pages/user/register-success");

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("pages/user/register");
            modelAndView.addObject("registerForm", registerForm);
            return modelAndView;
        }

        // kiểm tra user đã tồn tại chưa
        if (userRepository.existsUserByEmailOrPhone(registerForm.getEmail(), registerForm.getPhone())) {
            modelAndView.setViewName("pages/user/register");
            modelAndView.addObject("errorMessage", "Tài khoản đã tồn tại");
            modelAndView.addObject("registerForm", registerForm);
            return modelAndView;
        }

        // kiểm tra password và rePassword
        if (!matchPassword(registerForm.getPassword(), registerForm.getRePassword())) {
            modelAndView.setViewName("pages/user/register");
            modelAndView.addObject("errorMessage", "Mật khẩu không giống nhau");
            modelAndView.addObject("registerForm", registerForm);
            return modelAndView;
        }

        // mapping data
        String passwordEncode = bCryptPasswordEncoder.encode(registerForm.getPassword());

        Set<String> roles = new HashSet<>();
        roles.add(UserRole.USER);

        User user = new User(
                sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME, PrefixId.USER),
                registerForm.getFullName(),
                registerForm.getPhone(),
                registerForm.getEmail(),
                false,
                new Date(),
                "",
                passwordEncode,
                roles
        );

        userRepository.insert(user);

        // gửi email thông báo đăng ký thành công
        mailSenderService.sendEmailConfirmRegister(user.getEmail(), user.getFullName());

        modelAndView.addObject("email", user.getEmail());
        return modelAndView;
    }
    // endregion

    @GetMapping("/forgot-password")
    public ModelAndView forgotPasswordPage() {
        ModelAndView modelAndView = new ModelAndView("pages/user/forgot-password");
        return modelAndView;
    }

    @GetMapping("/reset-password")
    public ModelAndView resetPasswordPage() {
        ModelAndView modelAndView = new ModelAndView("pages/user/reset-password");
        return modelAndView;
    }

    private boolean matchPassword(String password, String rePassword) {
        if (password.isBlank() && rePassword.isBlank()) {
            return false;
        }
        return password.equals(rePassword);
    }
}
