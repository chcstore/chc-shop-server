package com.chc.server.controllers;

import com.chc.server.annotations.RoleAdmin;
import org.springframework.context.annotation.Role;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @RoleAdmin
    @GetMapping
    public String adminPage() {
        return "redirect:/admin/dashboard";
    }

    @RoleAdmin
    @GetMapping("/dashboard")
    public ModelAndView dashboardPage() {
        ModelAndView modelAndView = new ModelAndView("pages/admin/dashboard");
        return modelAndView;
    }
}
