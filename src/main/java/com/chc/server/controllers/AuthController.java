package com.chc.server.controllers;

import com.chc.server.constants.SessionAttributeContext;
import com.chc.server.constants.UserRole;
import com.chc.server.jwt.JwtToken;
import com.chc.server.jwt.JwtUserDetailsService;
import com.chc.server.models.User;
import com.chc.server.models.forms.LoginForm;
import com.chc.server.repositories.UserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtToken jwtToken;

    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, JwtUserDetailsService jwtUserDetailsService, JwtToken jwtToken) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.jwtToken = jwtToken;
    }

    @GetMapping("/login")
    public ModelAndView loginPage() {
        ModelAndView modelAndView = new ModelAndView("pages/user/login");

        modelAndView.addObject("loginForm", new LoginForm());

        return modelAndView;
    }

    @PostMapping("/auth")
    public ModelAndView authProcessing(@Valid @ModelAttribute("loginForm") LoginForm loginForm,
                                       HttpSession session, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("redirect:/");

        User user = userRepository.findUserByEmail(loginForm.getEmail())
                .orElse(null);

        if (user == null || !bCryptPasswordEncoder.matches(loginForm.getPassword(), user.getPassword())) {
            modelAndView.addObject("loginForm", new LoginForm());
            modelAndView.addObject("errorMessage", "Tài khoản hoặc mật khẩu không chính xác");
            return modelAndView;
        }

        try {
            Authentication authentication = authenticate(loginForm.getEmail(), loginForm.getPassword());

            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            session.setAttribute(SessionAttributeContext.SECURITY_CONTEXT, securityContext);
        } catch (Exception e) {
            modelAndView.addObject("loginForm", new LoginForm());
            modelAndView.addObject("errorMessage", "Không thể đăng nhập vào lúc này, vui lòng thử lại sau");
            return modelAndView;
        }

        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(loginForm.getEmail());
        final String token = jwtToken.generateToken(userDetails, true);
        session.setAttribute(SessionAttributeContext.TOKEN, token);
        session.setAttribute(SessionAttributeContext.IS_ADMIN, user.getRoles().contains(UserRole.ADMIN));
        session.setAttribute(SessionAttributeContext.USER_FULLNAME, user.getFullName());

        return modelAndView;
    }

    @GetMapping("/logout")
    public String logoutProcessing(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }

    private Authentication authenticate(String username, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
