package com.chc.server.controllers;

import com.chc.server.annotations.RoleAdmin;
import com.chc.server.constants.PrefixId;
import com.chc.server.helpers.PaginationHelper;
import com.chc.server.models.Brand;
import com.chc.server.models.Supplier;
import com.chc.server.repositories.BrandRepository;
import com.chc.server.services.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class BrandController {
    private final BrandRepository brandRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    public BrandController(BrandRepository brandRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.brandRepository = brandRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    // region List
    @RoleAdmin
    @GetMapping("/admin/brands")
    public ModelAndView managementPage(@RequestParam(required = false) String q,
                                       @RequestParam(required = false) Integer page) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/brand/brand-management");
        int pageSize = PaginationHelper.getInstance().PAGE_SIZE;
        List<Brand> brands = new ArrayList<>();
        int totalPage = 0;

        if (q != null && !q.isEmpty()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            brands = brandRepository.findAllBy(criteria);
        } else {
            page = page == null ? 0 : Math.max(page - 1, 0);
            Pageable pageable = PageRequest.of(page, pageSize);
            Page<Brand> brandPage = brandRepository.findAll(pageable);

            brands = brandPage.getContent();
            totalPage = brandPage.getTotalPages();
        }

        modelAndView.addObject("brands", brands);
        modelAndView.addObject("totalPage", totalPage);

        return modelAndView;
    }
    // endregion

    // region Detail
    @RoleAdmin
    @GetMapping("/admin/brands/{id}")
    public ModelAndView detailPage(@PathVariable String id, RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/brand/brand-editor");

        Optional<Brand> brandOptional = brandRepository.findById(id);

        if (brandOptional.isPresent()) {
            modelAndView.addObject("brand", brandOptional.get());
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Thương hiệu không tồn tại");
            modelAndView.setViewName("redirect:/admin/brands");
        }

        return modelAndView;
    }
    // endregion

    // region Create or Update
    @RoleAdmin
    @GetMapping("/admin/brands/create")
    public ModelAndView createPage() {
        ModelAndView modelAndView = new ModelAndView("pages/admin/brand/brand-editor");

        Brand brand = new Brand();
        modelAndView.addObject("brand", brand);

        return modelAndView;
    }

    @RoleAdmin
    @PostMapping("/admin/brands/create")
    public ModelAndView createProcessing(@ModelAttribute("brand") @Valid Brand brand, BindingResult bindingResult,
                                         RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/brands");

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("pages/admin/brand/brand-editor");
            modelAndView.addObject("brand", brand);
            return modelAndView;
        }

        if (brand.getBrandId() == null || brand.getBrandId().isEmpty()) {
            brand.setBrandId(sequenceGeneratorService.generateSequence(Brand.SEQUENCE_NAME, PrefixId.BRAND));
            brandRepository.insert(brand);
            redirectAttributes.addFlashAttribute("successMessage", "Thêm thương hiệu thành công");
        } else {
            Optional<Brand> brandOptional = brandRepository.findById(brand.getBrandId());
            if (brandOptional.isPresent()) {
                Brand brandFind = brandOptional.get();

                brandFind.setBrandName(brand.getBrandName());
                brandFind.setDescription(brand.getDescription());
                brandRepository.save(brandFind);

                redirectAttributes.addFlashAttribute("successMessage", "Lưu thương hiệu thành công");
            } else {
                redirectAttributes.addFlashAttribute("errorMessage", "Thương hiệu không tồn tại");
                modelAndView.setViewName("redirect:/admin/brands");
            }
        }

        return modelAndView;
    }
    // endregion

    // region Delete
    @RoleAdmin
    @PostMapping("/admin/brands/{id}/delete")
    public String deleteSupplierProcessing(@PathVariable String id, RedirectAttributes redirectAttributes) {
        Optional<Brand> brandOptional = brandRepository.findById(id);

        if (brandOptional.isPresent()) {
            Brand brand = brandOptional.get();
            brandRepository.delete(brand);
            redirectAttributes.addFlashAttribute("successMessage", "Xóa thương hiệu thành công");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Thương hiệu không tồn tại");
        }

        return "redirect:/admin/brands";
    }
    // endregion
}
