package com.chc.server.controllers;

import com.chc.server.constants.SessionAttributeContext;
import com.chc.server.jwt.JWTAuthenticationService;
import com.chc.server.models.Cart;
import com.chc.server.models.Product;
import com.chc.server.repositories.ProductRepository;
import com.chc.server.utils.CartSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class CartController{
    private final ProductRepository productRepository;
    private String errorMess = "";

    public CartController(ProductRepository productRepository, JWTAuthenticationService authenticationService) {
        this.productRepository = productRepository;
    }

    @GetMapping("/cart")
    public ModelAndView cartPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("pages/user/cart");

        Cart chcCart = CartSession.getCartInSession(session);
        CartSession.updateCartQuantity(session);

        modelAndView.addObject("chcCart", chcCart);
        modelAndView.addObject("errorMess", errorMess);

        return modelAndView;
    }

    @PostMapping("/addToCart")
    public ModelAndView addItem(@RequestParam(value = "pid", defaultValue = "") String pid, HttpSession session,
                                @RequestParam(value = "quantity", required = false, defaultValue = "1") int quantity,
                                @RequestParam(value = "fromPage", required = true, defaultValue = "/") String fromPage){
        ModelAndView modelAndView = new ModelAndView("redirect:/");
        Product product = null;
        if(pid!=null && pid.length() > 0){
            product = productRepository.findById(pid).get();
        }
        if(product!=null){
            Cart chcCart = CartSession.getCartInSession(session);
            chcCart.addCartItem(product, quantity);
            CartSession.updateCartQuantity(session);
        }
        if(!fromPage.equals("/")){
            modelAndView.setViewName("redirect:/product/" + pid);
        }
        return modelAndView;
    }

    @PostMapping("/cart/update")
    public ModelAndView updateQuantity(HttpSession session, @RequestParam("pid") String pid, @RequestParam("quantity") int quantity){
        ModelAndView modelAndView = new ModelAndView("redirect:/cart");
        Cart chcCart = CartSession.getCartInSession(session);
        Optional<Product> optionalProduct = productRepository.findById(pid);
        if(optionalProduct.isPresent()){
            Product p = optionalProduct.get();
            if(quantity <= p.getQuantity()){
                errorMess = "";
                chcCart.updateCartItem(pid, quantity);
            }
            else{
                errorMess = "Sản phẩm " + p.getProductName() + " có số lượng tối đa được mua là " + p.getQuantity();

            }
        }
        return modelAndView;
    }

    @PostMapping("/cart/remove")
    public String remove(HttpSession session, @RequestParam(value = "pid", defaultValue = "") String pid){
        Product product = null;
        if(pid!=null && pid.length() > 0){
            product = productRepository.findById(pid).get();
        }
        if(product!=null){
            Cart cart = CartSession.getCartInSession(session);
            cart.removeCartItem(pid);
        }
        return "redirect:/cart";
    }
}
