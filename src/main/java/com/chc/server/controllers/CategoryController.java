package com.chc.server.controllers;

import com.chc.server.annotations.RoleAdmin;
import com.chc.server.constants.PrefixId;
import com.chc.server.helpers.PaginationHelper;
import com.chc.server.models.Category;
import com.chc.server.models.Product;
import com.chc.server.repositories.CategoryRepository;
import com.chc.server.repositories.ProductRepository;
import com.chc.server.services.SequenceGeneratorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class CategoryController {
    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    public CategoryController(CategoryRepository categoryRepository, ProductRepository productRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    // region List
    @RoleAdmin
    @GetMapping("/admin/categories")
    public ModelAndView managementPage(@RequestParam(required = false) String q,
                                       @RequestParam(required = false) Integer page) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/category/category-management");
        int pageSize = PaginationHelper.getInstance().PAGE_SIZE;
        List<Category> categories = new ArrayList<>();
        int totalPage = 0;

        if (q != null && !q.isEmpty()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            categories = categoryRepository.findAllBy(criteria);
        } else {
            page = page == null ? 0 : Math.max(page - 1, 0);
            Pageable pageable = PageRequest.of(page, pageSize);
            Page<Category> categoryPage = categoryRepository.findAll(pageable);

            categories = categoryPage.getContent();
            totalPage = categoryPage.getTotalPages();
        }

        modelAndView.addObject("categories", categories);
        modelAndView.addObject("totalPage", totalPage);

        return modelAndView;
    }
    // endregion

    // region Detail
    @RoleAdmin
    @GetMapping("/admin/categories/{id}")
    public ModelAndView detailPage(@PathVariable String id, RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/category/category-editor");

        Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (categoryOptional.isPresent()) {
            modelAndView.addObject("category", categoryOptional.get());
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Danh mục không tồn tại");
            modelAndView.setViewName("redirect:/admin/categories");
        }

        return modelAndView;
    }
    // endregion

    // region Create or Update
    @RoleAdmin
    @GetMapping("/admin/categories/create")
    public ModelAndView createPage() {
        ModelAndView modelAndView = new ModelAndView("pages/admin/category/category-editor");

        Category category = new Category();
        modelAndView.addObject("category", category);

        return modelAndView;
    }

    @RoleAdmin
    @PostMapping("/admin/categories/create")
    public ModelAndView createProcessing(@Valid @ModelAttribute("category") Category category, BindingResult bindingResult,
                                         RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/categories");

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("pages/admin/category/category-editor");
            modelAndView.addObject("category", category);
            return modelAndView;
        }

        if (category.getCategoryId() == null || category.getCategoryId().isEmpty()) {
            category.setCategoryId(sequenceGeneratorService.generateSequence(Category.SEQUENCE_NAME, PrefixId.CATEGORY));
            categoryRepository.insert(category);
            redirectAttributes.addFlashAttribute("successMessage", "Thêm danh mục thành công");
        } else {
            Optional<Category> categoryOptional = categoryRepository.findById(category.getCategoryId());
            if (categoryOptional.isPresent()) {
                Category categoryFind = categoryOptional.get();

                categoryFind.setCategoryName(category.getCategoryName());
                categoryFind.setDescription(category.getDescription());
                categoryRepository.save(categoryFind);
                redirectAttributes.addFlashAttribute("successMessage", "Lưu danh mục thành công");
            } else {
                redirectAttributes.addFlashAttribute("errorMessage", "Danh mục không tồn tại");
                modelAndView.setViewName("redirect:/admin/categories");
            }
        }

        return modelAndView;
    }
    // endregion

    // region Delete
    @RoleAdmin
    @PostMapping("/admin/categories/{id}/delete")
    public String deleteProcessing(@PathVariable String id, RedirectAttributes redirectAttributes) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (categoryOptional.isPresent()) {
            Category category = categoryOptional.get();
            categoryRepository.delete(category);
            redirectAttributes.addFlashAttribute("successMessage", "Xóa danh mục thành công");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Danh mục không tồn tại");
        }

        return "redirect:/admin/categories";
    }
    // endregion

    // region List Product of Category
    @GetMapping("/category")
    public ModelAndView productsOfCategory(@RequestParam(required = true) String id) {
        ModelAndView modelAndView = new ModelAndView("index");
        List<Product> products = new ArrayList<>();
        List<Category> categories = categoryRepository.findAll();

        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (categoryOptional.isPresent()) {
            Category category = categoryOptional.get();
            modelAndView.addObject("category", category);
            products = productRepository.findAllByCategoryCategoryIdAndPublishedIsTrueAndQuantityIsGreaterThan(category.getCategoryId(), 1);

            if (products.size() == 0) {
                modelAndView.addObject("message", "Danh mục chưa có sản phẩm");
            }
        } else {
            modelAndView.addObject("errorMessage", "Danh mục này không tồn tại");
        }

        modelAndView.addObject("products", products);
        modelAndView.addObject("categories", categories);

        return modelAndView;
    }
    // endregion

}
