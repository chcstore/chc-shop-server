package com.chc.server.controllers;

import com.chc.server.annotations.RoleUser;
import com.chc.server.constants.PrefixId;
import com.chc.server.jwt.JWTAuthenticationService;
import com.chc.server.models.*;
import com.chc.server.repositories.OrderRepository;
import com.chc.server.repositories.ProductRepository;
import com.chc.server.repositories.UserRepository;
import com.chc.server.services.MailSenderService;
import com.chc.server.services.SequenceGeneratorService;
import com.chc.server.types.OrderStatus;
import com.chc.server.utils.CartSession;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Controller
public class CheckoutController {
    private final OrderRepository orderRepository;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final JWTAuthenticationService authenticationService;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final MailSenderService mailSenderService;

    public CheckoutController(OrderRepository orderRepository, SequenceGeneratorService sequenceGeneratorService, JWTAuthenticationService authenticationService, UserRepository userRepository, ProductRepository productRepository, MailSenderService mailSenderService) {
        this.orderRepository = orderRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.authenticationService = authenticationService;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.mailSenderService = mailSenderService;
    }

    @GetMapping("/checkout")
    public ModelAndView checkoutPage(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("pages/user/checkout");
        Cart chcCart = CartSession.getCartInSession(session);

        // Check xem đã đăng nhập chưa. Chưa thì chuyển sang trang login.
        String email = authenticationService.getUserNameByAuthentication(session);
        if(email == null){
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }
        if(chcCart.isEmpty()){
            modelAndView.setViewName("redirect:/cart");
            return modelAndView;
        }

        // Else: Đã login + có sản phẩm trong cart rồi thì hiển trang checkout ra.
        Order order = new Order();
        Set<OrderDetail> orderDetails = new HashSet<OrderDetail>();
        chcCart.getCartItems().forEach(cartItem -> {
            OrderDetail od = new OrderDetail();
            od.setProduct(cartItem.getProduct());
            od.setQuantity(cartItem.getQuantity());
            od.setUnitPrice(cartItem.getProduct().getPrice());
            orderDetails.add(od);
        });

        // set CartItem vô OrderDetails
        order.setOrderDetails(orderDetails);
        // set Total cho Order
        order.setTotal(chcCart.calculateTotal());
        // set User khách hàng
        User user = userRepository.findUserByEmail(email).get();
        order.setUser(user);

        modelAndView.addObject("order",order);
        modelAndView.addObject("chcCart",chcCart);

        return modelAndView;
    }

    @RoleUser
    @PostMapping("/checkout")
    public ModelAndView checkoutProcessing(@ModelAttribute("order") @Valid Order order, Errors errors, HttpSession session, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView("redirect:/checkout/thank-you");
        Cart chcCart = CartSession.getCartInSession(session);

        // set orderDetails cho order
        Set<OrderDetail> orderDetails = new HashSet<OrderDetail>();
        chcCart.getCartItems().forEach(cartItem -> {
            OrderDetail od = new OrderDetail();
            od.setProduct(cartItem.getProduct());
            od.setQuantity(cartItem.getQuantity());
            od.setUnitPrice(cartItem.getProduct().getPrice());
            orderDetails.add(od);
        });
        order.setOrderDetails(orderDetails);

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("pages/user/checkout");
            //modelAndView.addObject("order", order);
            return modelAndView;
        }

        // set Id cho Order
        order.setOrderId(sequenceGeneratorService.generateSequence(Order.SEQUENCE_NAME, PrefixId.ORDER));

        // set orderDate là ngày-giờ hiện tại khách hàng order
        order.setOrderDate(new Date());

        // set Status cho order mới tạo là PENDING (Chờ xử lý)
        order.setStatus(OrderStatus.PENDING);

        // xử lý user
        Optional<User> userOptional = userRepository.findUserByEmail(order.getUser().getEmail());
        if(userOptional.isPresent()){
            order.setUser(userOptional.get());
        }

        // insert order xuống DB
        orderRepository.insert(order);

        // update quantity tồn kho của product
        for(OrderDetail od : order.getOrderDetails()){
            Optional<Product> productOptional = productRepository.findById(od.getProduct().getProductId());
            if(productOptional.isPresent()){
                Product product = productOptional.get();
                product.setQuantity(product.getQuantity() - od.getQuantity());
                System.out.println(product);
            }
        }

        // save thông tin order lần cuối đã xác nhận
        Optional<Order> lastOrderedOptional = orderRepository.findById(order.getOrderId());
        if(lastOrderedOptional.isPresent()){
            CartSession.storeLastOrderedInSession(session, lastOrderedOptional.get());
        }

        // xoá sessionAttribute
        CartSession.removeCartInSession(session);

        // gửi email thông tin đơn hàng
        mailSenderService.sendEmailConfirmOrder(order.getUser().getEmail(), order);

        return modelAndView;
    }

    @RoleUser
    @GetMapping("/checkout/thank-you")
    public ModelAndView redirectedThankYouPage(HttpSession session){
        ModelAndView modelAndView = new ModelAndView("pages/user/thankyou");
        Order lastOrderedCart = CartSession.getLastOrderedInSession(session);
        if (lastOrderedCart == null) {
            modelAndView.setViewName("redirect:/cart");
            return modelAndView;
        }
        modelAndView.addObject("order", lastOrderedCart);
        return modelAndView;
    }

}
