package com.chc.server.controllers;

import com.chc.server.exceptions.ErrorException;
import com.chc.server.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleNotFoundException(NotFoundException exception) {
        ModelAndView modelAndView = new ModelAndView("pages/errors/500");
        modelAndView.addObject("errorMessage", exception.getMessage());
        return modelAndView;
    }

    @ExceptionHandler(ErrorException.class)
    public ModelAndView handleErrorException(ErrorException exception) {
        ModelAndView modelAndView = new ModelAndView("pages/errors/500");
        modelAndView.addObject("errorMessage", exception.getMessage());
        return modelAndView;
    }

    @GetMapping("/not-authorized")
    @ExceptionHandler(AuthenticationException.class)
    public ModelAndView handleAuthenticationException(AuthenticationException exception) {
        ModelAndView modelAndView = new ModelAndView("pages/errors/not-authorized");
        modelAndView.addObject("errorMessage", exception.getMessage());
        return modelAndView;
    }
}
