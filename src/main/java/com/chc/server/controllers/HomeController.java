package com.chc.server.controllers;

import com.chc.server.models.Category;
import com.chc.server.models.Product;
import com.chc.server.repositories.CategoryRepository;
import com.chc.server.repositories.ProductRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/")
public class HomeController {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public HomeController(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping
    public ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView("index");

        List<Product> products = productRepository.findAllByPublishedIsTrueAndQuantityIsGreaterThan(1);
        List<Category> categories = categoryRepository.findAll();

        modelAndView.addObject("products", products);
        modelAndView.addObject("categories", categories);

        return modelAndView;
    }


}
