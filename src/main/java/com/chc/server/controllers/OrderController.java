package com.chc.server.controllers;

import com.chc.server.annotations.RoleAdmin;
import com.chc.server.helpers.PaginationHelper;
import com.chc.server.models.Order;
import com.chc.server.repositories.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class OrderController {
    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @RoleAdmin
    @GetMapping("/admin/orders")
    public ModelAndView managementPage(@RequestParam(required = false) String q,
                                       @RequestParam(required = false) Integer page) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/order/order-management");
        int pageSize = PaginationHelper.getInstance().PAGE_SIZE;
        List<Order> orders = new ArrayList<>();
        int totalPage = 0;
        Sort sort = Sort.by(Sort.Direction.DESC, "orderDate");

        if (q != null && !q.isEmpty()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            orders = orderRepository.findAllBy(criteria);
        } else {
            page = page == null ? 0 : Math.max(page - 1, 0);
            Pageable pageable = PageRequest.of(page, pageSize, sort);
            Page<Order> orderPage = orderRepository.findAll(pageable);

            orders = orderPage.getContent();
            totalPage = orderPage.getTotalPages();
        }

        modelAndView.addObject("orders", orders);
        modelAndView.addObject("totalPage", totalPage);

        return modelAndView;
    }

    @RoleAdmin
    @GetMapping("/admin/orders/{id}")
    public ModelAndView detailPage(@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/order/order-detail");

        Optional<Order> orderOptional = orderRepository.findById(id);

        if (orderOptional.isPresent()) {
            modelAndView.addObject("order", orderOptional.get());
        } else {
            modelAndView.addObject("order", new Order());
            modelAndView.addObject("errorMessage", "Đơn hàng này không tồn tại !");
        }

        return modelAndView;
    }

    @GetMapping("/orders")
    public ModelAndView ordersPage() {
        ModelAndView modelAndView = new ModelAndView("pages/user/orders");
        return modelAndView;
    }
}
