package com.chc.server.controllers;

import com.chc.server.annotations.RoleAdmin;
import com.chc.server.constants.PrefixId;
import com.chc.server.exceptions.NotFoundException;
import com.chc.server.helpers.PaginationHelper;
import com.chc.server.models.Brand;
import com.chc.server.models.Category;
import com.chc.server.models.Product;
import com.chc.server.models.Supplier;
import com.chc.server.models.forms.ProductForm;
import com.chc.server.repositories.*;
import com.chc.server.services.FileStorageService;
import com.chc.server.services.SequenceGeneratorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Controller
public class ProductController {
    private final ProductRepository productRepository;
    private final BrandRepository brandRepository;
    private final CategoryRepository categoryRepository;
    private final SupplierRepository supplierRepository;
    private final OrderRepository orderRepository;
    private final FileStorageService fileStorageService;
    private final SequenceGeneratorService sequenceGeneratorService;

    public ProductController(ProductRepository productRepository,
                             BrandRepository brandRepository,
                             CategoryRepository categoryRepository,
                             SupplierRepository supplierRepository,
                             OrderRepository orderRepository, FileStorageService fileStorageService,
                             SequenceGeneratorService sequenceGeneratorService) {
        this.productRepository = productRepository;
        this.brandRepository = brandRepository;
        this.categoryRepository = categoryRepository;
        this.supplierRepository = supplierRepository;
        this.orderRepository = orderRepository;
        this.fileStorageService = fileStorageService;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    // region List
    @RoleAdmin
    @GetMapping("/admin/products")
    public ModelAndView managementPage(@RequestParam(required = false) String q,
                                       @RequestParam(required = false) Integer page) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/product/product-management");
        int pageSize = PaginationHelper.getInstance().PAGE_SIZE;
        List<Product> products = new ArrayList<>();
        int totalPage = 0;

        if (q != null && !q.isEmpty()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            products = productRepository.findAllBy(criteria);
        } else {
            page = page == null ? 0 : Math.max(page - 1, 0);
            Pageable pageable = PageRequest.of(page, pageSize);
            Page<Product> productPage = productRepository.findAll(pageable);

            products = productPage.getContent();
            totalPage = productPage.getTotalPages();
        }

        modelAndView.addObject("products", products);
        modelAndView.addObject("totalPage", totalPage);

        return modelAndView;
    }
    // endregion

    // region Detail
    @RoleAdmin
    @GetMapping("/admin/products/{id}")
    public ModelAndView detailPage(@PathVariable String id, RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/product/product-editor");

        Optional<Product> productOptional = productRepository.findById(id);

        if (productOptional.isPresent()) {
            ProductForm productForm = convertFromProduct(productOptional.get());

            modelAndView.addObject("productForm", productForm);
            modelAndView.addObject("categories", categoryRepository.findAll());
            modelAndView.addObject("brands", brandRepository.findAll());
            modelAndView.addObject("suppliers", supplierRepository.findAll());
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Sản phẩm không tồn tại");
            modelAndView.setViewName("redirect:/admin/products");
        }

        return modelAndView;
    }

    @GetMapping("/product/{productId}")
    public ModelAndView productDetailPage(@PathVariable String productId) {
        ModelAndView modelAndView = new ModelAndView("pages/user/product-detail");

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Sản phẩm không tồn tại"));

        modelAndView.addObject("product", product);

        return modelAndView;
    }
    // endregion

    // region Create or Update
    @RoleAdmin
    @GetMapping("/admin/products/create")
    public ModelAndView createPage() {
        ModelAndView modelAndView = new ModelAndView("pages/admin/product/product-editor");

        ProductForm productForm = new ProductForm();
        modelAndView.addObject("productForm", productForm);

        modelAndView.addObject("categories", categoryRepository.findAll());
        modelAndView.addObject("brands", brandRepository.findAll());
        modelAndView.addObject("suppliers", supplierRepository.findAll());

        return modelAndView;
    }

    @RoleAdmin
    @PostMapping("/admin/products/create")
    public ModelAndView createProcessing(@Valid @ModelAttribute("productForm") ProductForm productForm,
                                         BindingResult bindingResult,
                                         @RequestParam("file") MultipartFile file,
                                         RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/products");
        Product product = new Product();

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("pages/admin/product/product-editor");

            modelAndView.addObject("productForm", productForm);
            modelAndView.addObject("categories", categoryRepository.findAll());
            modelAndView.addObject("brands", brandRepository.findAll());
            modelAndView.addObject("suppliers", supplierRepository.findAll());

            return modelAndView;
        }

        // upload image
        if (file != null && Objects.requireNonNull(file.getContentType()).startsWith("image")) {
            String imagePath = fileStorageService.storeFile(file);
            productForm.setImageUrl(imagePath);
        }

        // convert product form to product
        try {
            product = convertFromProductForm(productForm);
        } catch (NotFoundException e) {
            return errorMessage(productForm, e.getMessage());
        }

        if (product.getProductId() == null || product.getProductId().isEmpty()) {
            product.setProductId(sequenceGeneratorService.generateSequence(Product.SEQUENCE_NAME, PrefixId.PRODUCT));
            productRepository.insert(product);
            redirectAttributes.addFlashAttribute("successMessage", "Thêm sản phẩm thành công");
        } else {
            Optional<Product> productOptional = productRepository.findById(productForm.getProductId());
            if (productOptional.isPresent()) {
                Product productFind = productOptional.get();

                productFind.setProductId(product.getProductId());
                productFind.setProductName(product.getProductName());
                productFind.setPrice(product.getPrice());
                productFind.setDescription(product.getDescription());
                productFind.setImageUrl(product.getImageUrl() != null ? product.getImageUrl() : productFind.getImageUrl());
                productFind.setQuantity(product.getQuantity());
                productFind.setWarrantyPeriod(product.getWarrantyPeriod());
                productFind.setOrigin(product.getOrigin());
                productFind.setSku(product.getSku());
                productFind.setPublished(product.isPublished());
                productFind.setBrand(product.getBrand());
                productFind.setSupplier(product.getSupplier());
                productFind.setCategory(product.getCategory());

                productRepository.save(productFind);

                redirectAttributes.addFlashAttribute("successMessage", "Lưu sản phẩm thành công");
            } else {
                redirectAttributes.addFlashAttribute("errorMessage", "Sản phẩm không tồn tại");
                modelAndView.setViewName("redirect:/admin/products");
            }
        }
        return modelAndView;
    }
    // endregion

    // region Delete
    @RoleAdmin
    @PostMapping("/admin/products/{id}/delete")
    public String deleteProcessing(@PathVariable String id, RedirectAttributes redirectAttributes) {
        Optional<Product> productOptional = productRepository.findById(id);

        if (productOptional.isPresent()) {
            Product product = productOptional.get();

            // kiểm tra sản phẩm có đang được đặt hàng không
            if (orderRepository.findAllByProductId(product.getProductId()).size() > 0) {
                redirectAttributes.addFlashAttribute("errorMessage", "Không thể xóa sản phẩm đang được đặt hàng");
            } else {
                redirectAttributes.addFlashAttribute("successMessage", "Xóa sản phẩm thành công");
                productRepository.delete(product);
            }

        }

        return "redirect:/admin/products";
    }
    // endregion

    private ModelAndView errorMessage(ProductForm productForm, String errorMessage) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("pages/admin/product/product-editor");
        modelAndView.addObject("errorMessage", "Sản phẩm không tồn tại");
        modelAndView.addObject("productForm", productForm);

        return modelAndView;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields("productId", "productName", "price", "description",
                "quantity", "warrantyPeriod", "origin", "sku", "published",
                "brandId", "categoryId", "supplierId", "file*");
    }

    private ProductForm convertFromProduct(Product product) {
        ProductForm productForm = new ProductForm();

        productForm.setProductId(product.getProductId());
        productForm.setProductName(product.getProductName());
        productForm.setPrice(product.getPrice());
        productForm.setDescription(product.getDescription());
        productForm.setImageUrl(product.getImageUrl());
        productForm.setQuantity(product.getQuantity());
        productForm.setWarrantyPeriod(product.getWarrantyPeriod());
        productForm.setOrigin(product.getOrigin());
        productForm.setSku(product.getSku());
        productForm.setPublished(product.isPublished());

        if (product.getBrand() != null) {
            productForm.setBrandId(product.getBrand().getBrandId());
        }

        if (product.getCategory() != null) {
            productForm.setCategoryId(product.getCategory().getCategoryId());
        }

        if (product.getSupplier() != null) {
            productForm.setSupplierId(product.getSupplier().getSupplierId());
        }

        return productForm;
    }

    private Product convertFromProductForm(ProductForm productForm) throws NotFoundException {
        Product product = new Product();

        product.setProductId(productForm.getProductId());
        product.setProductName(productForm.getProductName());
        product.setPrice(productForm.getPrice());
        product.setDescription(productForm.getDescription());
        product.setImageUrl(productForm.getImageUrl());
        product.setQuantity(productForm.getQuantity());
        product.setWarrantyPeriod(productForm.getWarrantyPeriod());
        product.setOrigin(productForm.getOrigin());
        product.setSku(productForm.getSku());
        product.setPublished(productForm.isPublished());

        Brand brand = brandRepository.findById(productForm.getBrandId())
                .orElseThrow(() -> new NotFoundException("Thương hiệu không tồn tại"));

        Supplier supplier = supplierRepository.findById(productForm.getSupplierId())
                .orElseThrow(() -> new NotFoundException("Nhà cung cấp không tồn tại"));

        Category category = categoryRepository.findById(productForm.getCategoryId())
                .orElseThrow(() -> new NotFoundException("Danh mục không tồn tại"));

        product.setBrand(brand);
        product.setSupplier(supplier);
        product.setCategory(category);

        return product;
    }
}
