package com.chc.server.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/profile")
public class ProfileController {
    @GetMapping
    public ModelAndView cartPage() {
        ModelAndView modelAndView = new ModelAndView("pages/user/profile");
        return modelAndView;
    }
}
