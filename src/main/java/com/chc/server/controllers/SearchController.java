package com.chc.server.controllers;

import com.chc.server.models.Category;
import com.chc.server.models.Product;
import com.chc.server.repositories.CategoryRepository;
import com.chc.server.repositories.ProductRepository;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public SearchController(ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/search")
    public ModelAndView searchProcessing(@RequestParam(required = false) String q) {
        ModelAndView modelAndView  = new ModelAndView("index");
        List<Product> products = new ArrayList<>();
        List<Category> categories = categoryRepository.findAll();

        if (!q.isBlank()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            products = productRepository.findAllByPublishedIsTrueAndQuantityIsGreaterThan(1, criteria);
        } else {
            products = productRepository.findAllByPublishedIsTrueAndQuantityIsGreaterThan(1);
        }

        modelAndView.addObject("categories", categories);
        modelAndView.addObject("products", products);
        return modelAndView;
    }
}
