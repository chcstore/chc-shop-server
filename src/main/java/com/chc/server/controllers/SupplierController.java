package com.chc.server.controllers;

import com.chc.server.annotations.RoleAdmin;
import com.chc.server.constants.PrefixId;
import com.chc.server.helpers.PaginationHelper;
import com.chc.server.models.Supplier;
import com.chc.server.repositories.SupplierRepository;
import com.chc.server.services.SequenceGeneratorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class SupplierController {
    private final SupplierRepository supplierRepository;
    private final SequenceGeneratorService sequenceGeneratorService;

    public SupplierController(SupplierRepository supplierRepository, SequenceGeneratorService sequenceGeneratorService) {
        this.supplierRepository = supplierRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    // region List
    @RoleAdmin
    @GetMapping("/admin/suppliers")
    public ModelAndView managementPage(@RequestParam(required = false) String q,
                                               @RequestParam(required = false) Integer page) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/supplier/supplier-management");
        int pageSize = PaginationHelper.getInstance().PAGE_SIZE;
        List<Supplier> suppliers = new ArrayList<>();
        int totalPage = 0;

        if (q != null && !q.isEmpty()) {
            TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(q);
            suppliers = supplierRepository.findAllBy(criteria);
        } else {
            page = page == null ? 0 : Math.max(page - 1, 0);
            Pageable pageable = PageRequest.of(page, pageSize);
            Page<Supplier> supplierPage = supplierRepository.findAll(pageable);

            suppliers = supplierPage.getContent();
            totalPage = supplierPage.getTotalPages();
        }

        modelAndView.addObject("suppliers", suppliers);
        modelAndView.addObject("totalPage", totalPage);

        return modelAndView;
    }
    // endregion

    // region Detail
    @RoleAdmin
    @GetMapping("/admin/suppliers/{id}")
    public ModelAndView detailPage(@PathVariable String id, RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("pages/admin/supplier/supplier-editor");

        Optional<Supplier> supplierOptional = supplierRepository.findById(id);

        if (supplierOptional.isPresent()) {
            modelAndView.addObject("supplier", supplierOptional.get());
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Nhà cung cấp không tồn tại");
            modelAndView.setViewName("redirect:/admin/suppliers");
        }

        return modelAndView;
    }
    // endregion

    // region Create or Update
    @RoleAdmin
    @GetMapping("/admin/suppliers/create")
    public ModelAndView createPage() {
        ModelAndView modelAndView = new ModelAndView("pages/admin/supplier/supplier-editor");

        Supplier supplier = new Supplier();
        modelAndView.addObject("supplier", supplier);

        return modelAndView;
    }

    @RoleAdmin
    @PostMapping("/admin/suppliers/create")
    public ModelAndView createProcessing(@Valid @ModelAttribute("supplier") Supplier supplier, BindingResult bindingResult,
                                         RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/suppliers");

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("pages/admin/supplier/supplier-editor");
            modelAndView.addObject("supplier", supplier);
            return modelAndView;
        }

        if (supplier.getSupplierId() == null || supplier.getSupplierId().isEmpty()) {
            supplier.setSupplierId(sequenceGeneratorService.generateSequence(Supplier.SEQUENCE_NAME, PrefixId.SUPPLIER));
            supplierRepository.insert(supplier);
            redirectAttributes.addFlashAttribute("successMessage", "Thêm nhà cung cấp thành công");
        } else {
            Optional<Supplier> supplierOptional = supplierRepository.findById(supplier.getSupplierId());
            if (supplierOptional.isPresent()) {
                Supplier supplierFind = supplierOptional.get();

                supplierFind.setSupplierName(supplier.getSupplierName());
                supplierFind.setContactPerson(supplier.getContactPerson());
                supplierFind.setPhone(supplier.getPhone());
                supplierFind.setAddress(supplier.getAddress());
                supplierFind.setDescription(supplier.getDescription());

                supplierRepository.save(supplierFind);
                redirectAttributes.addFlashAttribute("successMessage", "Lưu nhà cung cấp thành công");
            } else {
                redirectAttributes.addFlashAttribute("errorMessage", "Nhà cung cấp không tồn tại");
                modelAndView.setViewName("redirect:/admin/suppliers");
            }
        }

        return modelAndView;
    }
    // endregion

    // region Delete
    @RoleAdmin
    @PostMapping("/admin/suppliers/{id}/delete")
    public String deleteProcessing(@PathVariable String id, RedirectAttributes redirectAttributes) {
        System.out.println(id);
        Optional<Supplier> supplierOptional = supplierRepository.findById(id);

        if (supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            supplierRepository.delete(supplier);
            redirectAttributes.addFlashAttribute("successMessage", "Xóa nhà cung cấp thành công");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Nhà cung cấp không tồn tại");
        }

        return "redirect:/admin/suppliers";
    }
    // endregion
}
