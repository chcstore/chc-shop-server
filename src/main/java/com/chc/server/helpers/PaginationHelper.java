package com.chc.server.helpers;

public class PaginationHelper {
    private static PaginationHelper _instance;
    public final int PAGE_SIZE = 10;

    private PaginationHelper() {}

    public static PaginationHelper getInstance() {
        if (_instance == null) {
            synchronized (PaginationHelper.class) {
                if (_instance == null) {
                    _instance = new PaginationHelper();
                }
            }
        }

        return _instance;
    }

    public int totalPage(long total) {
        return (int) total/PAGE_SIZE;
    }
}
