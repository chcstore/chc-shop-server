package com.chc.server.models;

import com.chc.server.constants.CollectionName;
import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = CollectionName.BRAND)
@TypeAlias(value = CollectionName.BRAND)
public class Brand extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = String.format("%s_sequence", CollectionName.BRAND);

    @MongoId
    @TextIndexed(weight = 1)
    private String brandId;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotBlank(message = "Vui lòng nhập tên thương hiệu")
    private String brandName;

    @TextIndexed(weight = 3)
    @EqualsAndHashCode.Exclude
    private String description;
}

