package com.chc.server.models;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
public class Cart implements Serializable{
    private String orderId;
    private final List<CartItem> cartItems;
    private User customer;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<CartItem> getCartItems() {
        return this.cartItems;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public Cart() {
        cartItems = new ArrayList<>();
    }

    public CartItem findItemByProductID(String productId){
        for(CartItem item : cartItems){
            if(item.getProduct().getProductId().equals(productId)){
                return item;
            }
        }
        return null;
    }

    /**
     * @param p
     * @param quantity
     */
    public void addCartItem(Product p, int quantity){
        CartItem item = findItemByProductID(p.getProductId());

        if (item != null){
            item.setQuantity(item.getQuantity() + quantity);
        } else {
            item = new CartItem(p);
            item.setQuantity(quantity);
            cartItems.add(item);
        }
    }

    /**
     * @param productId
     * @param quantity
     */
    public void updateCartItem(String productId, int quantity){
        CartItem cartItem = this.findItemByProductID(productId);

        if(cartItem!=null){
            if(quantity<=0){
                this.cartItems.remove(cartItem);
            }
            else{
                cartItem.setQuantity(quantity);
            }
        }
    }

    public void updateQuantity(Cart cart) {
        if (cart != null) {
            List<CartItem> cartItemList = cart.getCartItems();
            for (CartItem cartItem : cartItemList) {
                this.updateCartItem(cartItem.getProduct().getProductId(), cartItem.getQuantity());
            }
        }

    }

    public void removeCartItem(String productId){
        CartItem cartItem = this.findItemByProductID(productId);
        if(cartItem!=null){
            this.cartItems.remove(cartItem);
        }
    }

    public double calculateTotal(){
        double total = 0;
        for(CartItem item : this.cartItems)
            total += item.getSubTotal();
        return total;
    }

    public int getCartQuantity(){
        int quantity = 0;
        for(CartItem cartItem : this.cartItems){
            quantity += cartItem.getQuantity();
        }
        return quantity;
    }

    public boolean isEmpty() {
        return this.cartItems.isEmpty();
    }
}
