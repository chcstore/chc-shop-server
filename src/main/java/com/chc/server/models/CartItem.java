package com.chc.server.models;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

public class CartItem implements Serializable{
    private Product product;
    private int quantity;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public CartItem(Product product) {
        this.product = product;
        this.quantity = 1;
    }

    public CartItem() {
        this.quantity = 0;
    }

    public double getSubTotal(){
        return this.product.getPrice() * quantity;
    }
}
