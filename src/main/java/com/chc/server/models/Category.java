package com.chc.server.models;

import com.chc.server.constants.CollectionName;
import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = CollectionName.CATEGORY)
@TypeAlias(value = CollectionName.CATEGORY)
public class Category extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = String.format("%s_sequence", CollectionName.CATEGORY);

    @MongoId
    @TextIndexed(weight = 1)
    private String categoryId;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập tên danh mục")
    private String categoryName;

    @TextIndexed(weight = 3)
    @EqualsAndHashCode.Exclude
    private String description;
}
