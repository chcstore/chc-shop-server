package com.chc.server.models;

import com.chc.server.constants.CollectionName;
import lombok.Data;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document(collection = CollectionName.DATABASE_SEQUENCE)
@TypeAlias(value = CollectionName.DATABASE_SEQUENCE)
public class DatabaseSequence {
    @MongoId
    private String sequenceId;
    private long sequence;
}
