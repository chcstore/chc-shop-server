package com.chc.server.models;

import com.chc.server.constants.CollectionName;
import com.chc.server.types.OrderStatus;
import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.data.mongodb.core.mapping.TextScore;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = CollectionName.ORDER)
@TypeAlias(value = CollectionName.ORDER)
public class Order extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = String.format("%s_sequence", CollectionName.ORDER);

    @MongoId
    @TextIndexed(weight = 1)
    private String orderId;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    private Date orderDate;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    private Date shippedDate;

    @EqualsAndHashCode.Exclude
    private OrderStatus status;

    @EqualsAndHashCode.Exclude
    private double total;

    @EqualsAndHashCode.Exclude
    private double shippingFee;

    @EqualsAndHashCode.Exclude
    private String note;

    @TextIndexed(weight = 3)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập địa chỉ giao hàng")
    private String shippingAddress;

    private Set<OrderDetail> orderDetails;

    @DBRef
    private User user;

    public int getOrderQuantity(){
        int quantity = 0;
        for(OrderDetail od : this.orderDetails){
            quantity += od.getQuantity();
        }
        return quantity;
    }
}
