package com.chc.server.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class OrderDetail implements Serializable {
    @EqualsAndHashCode.Exclude
    @NotNull
    private double unitPrice;

    @EqualsAndHashCode.Exclude
    @NotNull
    private int quantity;

    @DBRef
    @NotNull
    private Product product;
}
