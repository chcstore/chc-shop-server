package com.chc.server.models;

import com.chc.server.constants.CollectionName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.data.mongodb.core.mapping.TextScore;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = CollectionName.PRODUCT)
@TypeAlias(value = CollectionName.PRODUCT)
public class Product extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = String.format("%s_sequence", CollectionName.PRODUCT);

    @MongoId
    @TextIndexed(weight = 1)
    private String productId;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập tên sản phẩm")
    private String productName;

    @EqualsAndHashCode.Exclude
    @NotNull(message = "Vui lòng nhập giá sản phẩm")
    private double price;

    @TextIndexed(weight = 4)
    @EqualsAndHashCode.Exclude
    private String description;

    @EqualsAndHashCode.Exclude
    private String imageUrl;

    @EqualsAndHashCode.Exclude
    @NotNull(message = "Vui lòng nhập số lượng sản phẩm")
    private int quantity;

    @EqualsAndHashCode.Exclude
    @NotNull(message = "Vui lòng nhập thời gian bảo hàng")
    private int warrantyPeriod;

    @TextIndexed(weight = 3)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập xuất xứ sản phẩm")
    private String origin;

    @TextIndexed(weight = 2)
    @NotEmpty(message = "Vui lòng nhập SKU sản phẩm")
    private String sku;

    @EqualsAndHashCode.Exclude
    private boolean published;

    @DBRef
    private Brand brand;

    @DBRef
    private Category category;

    @DBRef
    private Supplier supplier;


}
