package com.chc.server.models;


import com.chc.server.constants.CollectionName;
import lombok.*;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = CollectionName.SUPPLIER)
@TypeAlias(value = CollectionName.SUPPLIER)
public class Supplier extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = String.format("%s_sequence", CollectionName.SUPPLIER);

    @MongoId
    @TextIndexed(weight = 1)
    private String supplierId;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotBlank(message = "Vui lòng nhập tên nhà cung cấp")
    @Size(min = 1, max = 50, message = "Tên nhà cung cấp phải từ 1 đến 50 kí tự")
    private String supplierName;

    @TextIndexed(weight = 5)
    @EqualsAndHashCode.Exclude
    private String description;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotBlank(message = "Vui lòng nhập tên người liên hệ")
    @Size(min = 5, max = 30, message = "Tên người liên hệ phải từ 5 đến 30 kí tự")
    private String contactPerson;

    @TextIndexed(weight = 3)
    @EqualsAndHashCode.Exclude
    @NotBlank(message = "Vui lòng nhập số điện thoại")
    @Pattern(regexp = "^0[0-9]{9}$", message = "Vui lòng nhập số điện thoại")
    private String phone;

    @TextIndexed(weight = 4)
    @EqualsAndHashCode.Exclude
    @NotBlank(message = "Vui lòng nhập địa chỉ nhà cung cấp")
    @Size(min = 1, max = 250, message = "Địa chỉ không quá 250 kí tự")
    private String address;
}
