package com.chc.server.models;

import com.chc.server.constants.CollectionName;
import com.chc.server.constants.UserRole;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Document(collection = CollectionName.USER)
@TypeAlias(value = CollectionName.USER)
public class User extends AuditModel {
    @Transient
    public static final String SEQUENCE_NAME = String.format("%s_sequence", CollectionName.USER);

    @MongoId
    @TextIndexed(weight = 1)
    private String uid;

    @TextIndexed(weight = 2)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập họ tên")
    private String fullName;

    @TextIndexed(weight = 3)
    @Indexed(unique = true)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập số điện thoại")
    @Pattern(regexp = "^0[0-9]{10}$", message = "Vui lòng nhập số điện thoại")
    private String phone;

    @TextIndexed(weight = 3)
    @Indexed(unique = true)
    @EqualsAndHashCode.Exclude
    @NotEmpty(message = "Vui lòng nhập email")
    @Email(message = "Vui lòng nhập email")
    private String email;

    @EqualsAndHashCode.Exclude
    @NotNull(message = "Vui lòng chọn giới tính")
    private boolean female;

    @EqualsAndHashCode.Exclude
    @NotNull(message = "Vui lòng chọn ngày sinh")
    private Date birthday;

    @EqualsAndHashCode.Exclude
    private String address;

    @EqualsAndHashCode.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @EqualsAndHashCode.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<String> roles;

    public User() {
    }
}
