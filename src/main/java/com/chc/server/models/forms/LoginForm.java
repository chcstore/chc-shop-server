package com.chc.server.models.forms;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
public class LoginForm implements Serializable {
    @Email(message = "Vui lòng nhập email")
    @NotEmpty(message = "Vui lòng nhập email")
    private String email;

    @NotEmpty(message = "Vui lòng nhập mật khẩu")
    private String password;
}
