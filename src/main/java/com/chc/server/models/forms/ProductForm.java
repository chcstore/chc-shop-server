package com.chc.server.models.forms;

import com.chc.server.models.Product;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductForm extends Product {
    @NotBlank(message = "Vui lòng chọn danh mục sản phẩm")
    private String categoryId;

    @NotBlank(message = "Vui lòng chọn nhà cung cấp")
    private String supplierId;

    @NotBlank(message = "Vui lòng chọn thương hiệu sản phẩm")
    private String brandId;
}
