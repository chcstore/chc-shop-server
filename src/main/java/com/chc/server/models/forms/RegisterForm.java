package com.chc.server.models.forms;

import com.chc.server.models.User;
import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class RegisterForm implements Serializable  {
    @NotEmpty(message = "Vui lòng nhập họ tên")
    @Size(min = 5, max = 50, message = "Tên phải từ 5 đến 50 kí tự")
    private String fullName;

    @Email(message = "Vui lòng nhập email")
    @NotEmpty(message = "Vui lòng nhập email")
    private String email;

    @NotEmpty(message = "Vui lòng nhập số điện thoại")
    @Pattern(regexp = "^0[0-9]{9}$", message = "Vui lòng nhập số điện thoại")
    private String phone;

    @NotEmpty(message = "Vui lòng nhập mật khẩu")
    @Size(min = 6, max = 18, message = "Vui lòng nhập mật khẩu từ 8 đến 16 kí tự")
    private String password;

    @NotEmpty(message = "Vui lòng nhập lại mật khẩu")
    @Size(min = 6, max = 18, message = "Vui lòng nhập mật khẩu từ 8 đến 16 kí tự")
    private String rePassword;
}
