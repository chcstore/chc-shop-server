package com.chc.server.repositories;

import com.chc.server.models.Brand;
import com.chc.server.models.Supplier;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends MongoRepository<Brand, String> {
    List<Brand> findAllBy(TextCriteria textCriteria);
}
