package com.chc.server.repositories;

import com.chc.server.models.Brand;
import com.chc.server.models.Category;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {
    List<Category> findAllBy(TextCriteria textCriteria);
}
