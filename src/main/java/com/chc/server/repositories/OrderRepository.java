package com.chc.server.repositories;

import com.chc.server.models.Order;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends MongoRepository<Order, String> {
    List<Order> findAllBy(TextCriteria textCriteria);

    @Query("{$and: [{'orderDetails.product.$id': ?0},{'status': 'PENDING'}]}")
    List<Order> findAllByProductId(String productId);
}
