package com.chc.server.repositories;

import com.chc.server.models.Brand;
import com.chc.server.models.Product;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
    List<Product> findAllBy(TextCriteria textCriteria);
    List<Product> findAllByCategoryCategoryId(String categoryId);
    List<Product> findAllByPublishedIsTrueAndQuantityIsGreaterThan(int quantity);
    List<Product> findAllByPublishedIsTrueAndQuantityIsGreaterThan(int quantity, TextCriteria textCriteria);
    List<Product> findAllByCategoryCategoryIdAndPublishedIsTrueAndQuantityIsGreaterThan(String categoryId, int quantity);

}
