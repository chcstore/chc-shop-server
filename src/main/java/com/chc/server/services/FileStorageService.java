package com.chc.server.services;

import com.chc.server.configs.FileStoragePropertiesConfig;
import com.chc.server.exceptions.ErrorException;
import com.chc.server.exceptions.NotFoundException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    @Value("${file.filename.length}")
    private int fileNameLength;

    @Value("${file.url.prefix}")
    private String prefixUrl;

    @Autowired
    public FileStorageService(FileStoragePropertiesConfig fileStoragePropertiesConfig) {
        this.fileStorageLocation = Paths.get(fileStoragePropertiesConfig.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new ErrorException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = generateFileName(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new ErrorException("Tên file không hợp lệ");
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return String.format("%s/%s", prefixUrl, fileName);
        } catch (IOException ex) {
            throw new ErrorException("Không thể lưu file. Vui lòng thử lại sau");
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new NotFoundException(String.format("Không tìm thấy file %s", fileName));
            }
        } catch (MalformedURLException ex) {
            throw new NotFoundException(String.format("Không tìm thấy file %s", fileName));
        }
    }

    private String generateFileName(String fileNameOrigin) {
        String fileName = "";
        boolean fileExist = false;

        while (!fileExist) {
            // B1: generate file name
            String fileExtension = FilenameUtils.getExtension(fileNameOrigin);
            String generatedString = RandomStringUtils.randomAlphabetic(fileNameLength);
            fileName = StringUtils.cleanPath(String.format("%s.%s", generatedString, fileExtension));

            // B2: Kiểm tra filename đã tồn tại chưa
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            File file = new File(String.valueOf(targetLocation));
            fileExist = !file.exists();
        }

        return fileName;
    }
}
