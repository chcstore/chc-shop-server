package com.chc.server.services;

import com.chc.server.constants.PrefixId;
import com.chc.server.constants.UserRole;
import com.chc.server.models.User;
import com.chc.server.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import java.util.*;

@Component
public class InitDataService implements CommandLineRunner {
    private final UserRepository userRepository;
    private final SequenceGeneratorService sequenceGeneratorService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public InitDataService(UserRepository userRepository,
                           SequenceGeneratorService sequenceGeneratorService,
                           BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.sequenceGeneratorService = sequenceGeneratorService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        createDefaultUser("nguyenchicuong2402@gmail.com", "0945767420");
        createDefaultUser("chumin99@hotmail.com", "0945767421");
        createDefaultUser("ngochuyen17011999@gmail.com", "0945767422");
    }

    private void createDefaultUser(String email, String phone) {
        if (userRepository.existsUserByEmail(email)) {
            return;
        }

        String password = bCryptPasswordEncoder.encode("@chc12345");

        Set<String> roles = new HashSet<>();
        roles.add(UserRole.ADMIN);

        User user = new User(
                sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME, PrefixId.USER),
                "Admin CHC",
                phone,
                email,
                false,
                new Date(),
                "",
                password,
                roles
        );

        userRepository.save(user);
    }
}
