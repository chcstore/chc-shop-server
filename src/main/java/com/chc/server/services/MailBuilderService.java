package com.chc.server.services;

import com.chc.server.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailBuilderService {
    private final TemplateEngine templateEngine;

    @Autowired
    public MailBuilderService(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String buildMailRegisterSuccess(String email, String fullName) {
        Context context = new Context();
        context.setVariable("email", String.valueOf(email));
        context.setVariable("fullName", fullName);
        return templateEngine.process("email-templates/account-confirmation-email", context);
    }

    public String buildMailOrderSuccess(Order order) {
        Context context = new Context();
        context.setVariable("order", order);
        return templateEngine.process("email-templates/order-confirmation-email", context);
    }
}
