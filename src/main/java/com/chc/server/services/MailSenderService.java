package com.chc.server.services;

import com.chc.server.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {
    private final JavaMailSender javaMailSender;
    private final MailBuilderService mailBuilderService;

    @Autowired
    public MailSenderService(JavaMailSender javaMailSender, MailBuilderService mailBuilderService) {
        this.javaMailSender = javaMailSender;
        this.mailBuilderService = mailBuilderService;
    }

    /**
     * Gửi email đơn giản
     * @param recipient String
     * @param subject String
     * @param message String
     * @return boolean
     */
    public boolean sendSimpleEmail(String recipient, String subject, String message) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setTo(recipient);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(message);

        try {
            this.javaMailSender.send(simpleMailMessage);
            return true;
        } catch (MailException e) {
            return false;
        }
    }

    public void sendEmailConfirmRegister(String recipient, String fullName) {
        MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Thông báo đăng ký thành công");

            String content = mailBuilderService.buildMailRegisterSuccess(recipient, fullName);
            messageHelper.setText(content, true);
        };

        try {
            javaMailSender.send(mimeMessagePreparator);
        } catch (MailException ignored) {
        }
    }

    public void sendEmailConfirmOrder(String recipient, Order order) {
        MimeMessagePreparator mimeMessagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Thông báo đặt hàng thành công");

            String content = mailBuilderService.buildMailOrderSuccess(order);
            messageHelper.setText(content, true);
        };

        try {
            javaMailSender.send(mimeMessagePreparator);
        } catch (MailException ignored) {
        }
    }
}
