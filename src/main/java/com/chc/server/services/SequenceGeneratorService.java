package com.chc.server.services;

import com.chc.server.models.DatabaseSequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import java.util.Objects;

@Service
public class SequenceGeneratorService {
    private final MongoTemplate mongoTemplate;

    @Autowired
    public SequenceGeneratorService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public String generateSequence(String seqName, String prefix) {
        DatabaseSequence counter = mongoTemplate.findAndModify(
                new Query().addCriteria(Criteria.where("_id").is(seqName)),
                new Update().inc("sequence",1),
                new FindAndModifyOptions().returnNew(true).upsert(true),
                DatabaseSequence.class);
        long id = !Objects.isNull(counter) ? counter.getSequence() : 1;

        return String.format("%s%d", prefix, id);
    }
}
