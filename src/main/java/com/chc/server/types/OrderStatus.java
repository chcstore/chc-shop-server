package com.chc.server.types;

public enum OrderStatus {
    PENDING,
    DELIVERING,
    DELIVERED
}
