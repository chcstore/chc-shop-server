package com.chc.server.utils;

import com.chc.server.constants.SessionAttributeContext;
import com.chc.server.models.Cart;
import com.chc.server.models.Order;
import org.springframework.session.Session;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

@Component
@Transactional
public class CartSession {

    public static Cart getCartInSession(HttpSession session){
        Cart cart = (Cart) session.getAttribute(SessionAttributeContext.CART_SESSION);
        if(cart == null){
            cart = new Cart();
            setCartInSession(session, cart);
        }
        return cart;
    }
    public static void updateCartQuantity(HttpSession session){
        Cart cart = (Cart) session.getAttribute(SessionAttributeContext.CART_SESSION);
        session.setAttribute(SessionAttributeContext.CART_SESSION_QUANTITY, cart.getCartQuantity());
    }
    public static void setCartInSession(HttpSession session, Cart cart) {
        session.setAttribute(SessionAttributeContext.CART_SESSION, cart);
    }

    public static void removeCartInSession(HttpSession session){
        session.removeAttribute(SessionAttributeContext.CART_SESSION);
        session.setAttribute(SessionAttributeContext.CART_SESSION_QUANTITY, 0);
    }

    public static void storeLastOrderedInSession(HttpSession session, Order order) {
        session.setAttribute(SessionAttributeContext.LAST_ORDERED_SESSION, order);
    }

    public static Order getLastOrderedInSession(HttpSession session) {
        return (Order) session.getAttribute(SessionAttributeContext.LAST_ORDERED_SESSION);
    }
}
